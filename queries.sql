--#1

SELECT 	
	owners.*,
    COUNT(animals.owner_id) AS pet_quantity
FROM 
	owners
LEFT JOIN 
	animals ON animals.owner_id = owners.id
GROUP BY animals.owner_id
HAVING pet_quantity = 1;

--#2

SELECT 	
	owners.*,
    COUNT(animals.owner_id) AS pet_quantity
FROM 
	owners
LEFT JOIN animals ON animals.owner_id = owners.id
GROUP BY animals.owner_id
HAVING pet_quantity > 1;

--#3

SELECT 
	*
FROM 
	owners
WHERE
	YEAR(date(now())) - YEAR(DATE(dob)) > 15;

--#4

SELECT
	owners.*,
	act.total_pets_walk,
	act.total_pets_eat,
	act.pets_quantity
FROM 
	owners
LEFT JOIN (
	SELECT 
		SUM(activity.walk) AS total_pets_walk,
		SUM(activity.eat) AS total_pets_eat,
		COUNT(owner_id) AS pets_quantity,
		animals.*
	FROM 
		activity 
	LEFT JOIN 
		animals 
	ON activity.id = animals.id
	GROUP BY owner_id
) AS act ON act.owner_id = owners.id

--#5

DELETE 
	animals 
FROM 
	animals 
INNER JOIN 
	activity 
ON activity.id = animals.id
WHERE activity.walk > 10

--#6

SELECT 
	animals.*,
    type_of_animal.type
FROM 
	animals 
    INNER JOIN 
		type_of_animal 
	ON animals.type_id = type_of_animal.type_id
WHERE type_of_animal.type = 'Dog'